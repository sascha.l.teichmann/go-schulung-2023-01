package greeter

import (
	"fmt"
	"hello/greeter/second"
)

func init() {
	fmt.Println("hello from init")
}

func init() {
	fmt.Println("hello from init 2")
}

func hello() {
	fmt.Println("Hello from greeter")
}

func Greet() {
	hello()
	second.SecongGreet()
	gruss()
}
