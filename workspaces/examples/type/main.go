package main

import "fmt"

var (
	global int = 56
)

/*
var global int

func init() {
	global = 56
}
*/

func init() {
	fmt.Println(global)
	global = 72
}

func main() {
	var (
		name  uint32  = 420
		hurra uint64  = uint64(name)
		pi    float32 = 3.14
		piBig float64 = float64(pi)
	)
	fmt.Println(name, hurra, pi, piBig, global)
}
