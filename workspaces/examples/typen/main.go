package main

import "fmt"

type myType int

func (mt myType) println() {
	fmt.Println(mt)
	mt = 64
}

func (mt myType) hallo() {
	fmt.Println("hallo from myType", mt)
}

func (mt *myType) modify(other myType) {
	*mt = other
}

type theirType float32

func (tt theirType) hallo() {
	fmt.Println("hallo", tt)
}

type yourType struct {
	myType // embedding
	*theirType
}

func (yt yourType) hallo() {
	yt.myType.hallo()
	yt.theirType.hallo()
}

func main() {

	var mt myType

	const foo int = 42

	mt = myType(foo)
	mt.println()
	mt.modify(65)
	//(&mt).modify(69)
	mt.println()

	yt := yourType{myType: 10, theirType: new(theirType)}
	yt.println()
	yt.hallo()

}
