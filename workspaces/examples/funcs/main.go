package main

import "fmt"

func berechung(factor int) func(int) int {
	// factor hat closure-Bindung
	return func(value int) int {
		return factor * value
	}
}

func gruss(who string, n int64) (v int, factor float32) {
	for i, y := int64(1), 42; i <= n; i, y = i+1, y*2 {
		fmt.Println("hello, ", who, y)
	}
	v = 90
	factor = 0.12
	return // naked return
}

func main() {
	n := int64(3)
	factor := 56
	if _, factor := gruss("Peter", n); factor > 0.11 && factor < 19 {
		fmt.Println(factor)
	} else {
		fmt.Println(factor, "var")
		// factor
	}
	fmt.Println(factor, "xxx")

	//b := berechung(10) // b ist Funktion

	var b func(int) int = berechung(10)

	fmt.Println(b(2))
	fmt.Println(b(3))

	b = berechung(20)
	fmt.Println(b(2))
	fmt.Println(b(3))

}
