package main

import (
	"encoding/json"
	"fmt"
)

type server struct {
	//mu       sync.Mutex
	database map[string]string
	die      bool
	calls    chan func(*server)
}

func newServer() *server {
	return &server{
		database: map[string]string{},
		calls:    make(chan func(*server)),
	}
}

func (s *server) store(k, v string) {
	//s.mu.Lock()
	//defer s.mu.Unlock()
	s.calls <- func(s *server) {
		s.database[k] = v
	}
}

func (s *server) fetch(k string) string {
	//s.mu.Lock()
	//defer s.mu.Unlock()
	result := make(chan string)
	s.calls <- func(s *server) {
		result <- s.database[k]
	}
	return <-result
}

func (s *server) kill() {
	s.calls <- func(s *server) {
		s.die = true
	}
}

func (s *server) dumpJSON() string {

	result := make(chan string)
	s.calls <- func(s *server) {
		bytes, err := json.Marshal(s.database)
		_ = err
		result <- string(bytes)
	}
	return <-result

}

func (s *server) run(done chan struct{}) {
	defer close(done)
	for call := range s.calls {
		call(s)
		if s.die {
			break
		}
	}
}

func main() {
	s := newServer()

	done := make(chan struct{})

	go s.run(done)

	go func() {
		s.store("Hello", "World")
		fmt.Println(s.fetch("Hello"))
		fmt.Println(s.dumpJSON())
		s.kill()
	}()

	<-done
}
