package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	var counter int32

	var wg sync.WaitGroup

	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for j := 0; j < 100_000; j++ {
				atomic.AddInt32(&counter, 1)
			}
		}()
	}

	//time.Sleep(5 * time.Second)
	wg.Wait()
	fmt.Println(counter)
}
