package main

import (
	"io"
	"log"
	"os"
)

type tr struct {
	xxx io.Reader
}

func (t *tr) Read(b []byte) (int, error) {
	n, err := t.xxx.Read(b)
	for i, c := range b[:n] {
		if c == 'a' {
			b[i] = 'A'
		}
	}
	return n, err
}

func main() {

	if _, err := io.Copy(os.Stdout, &tr{xxx: os.Stdin}); err != nil {
		log.Printf("error: %v\n", err)
	}
}
