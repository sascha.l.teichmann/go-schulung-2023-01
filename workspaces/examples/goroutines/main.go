package main

import (
	"fmt"
)

func main() {

	//var done chan bool
	//done = make(chan bool)
	done := make(chan bool, 1)

	done <- true

	go func() {
		// defer func() { done <- false }()
		// defer close(done)

		fmt.Println("Hello World")
	}()

	fmt.Println("Hello World")

	x, ok := <-done

	fmt.Println(x, ok)

	//time.Sleep(time.Microsecond)
}
