package main

import (
	"fmt"
	"strings"
)

func f() int { return 12 }

func main() {
	switch x := f(); x {
	case 12, 13:
		fmt.Println("zwoelf")
	case 14:
		fmt.Println("14")
	}

	const foo = "barabara"

	switch contains := func(s string) bool {
		return strings.Contains(foo, s)
	}; {
	case contains("ba"):
		fmt.Println("eins")
	case contains("ra"):
		fmt.Println("zwei")

	}
}
