package main

// Err-lang

import (
	"errors"
	"fmt"
	"log"
)

func calculate(a, b float64) (float64, error) {
	if b == 0 {
		return 0, errors.New("division by zero")
	}
	return a / b, nil
}

func main() {

	var err error
	calc := func(a, b float64) {
		if err == nil {
			var x float64
			x, err = calculate(a, b)
			if err == nil {
				fmt.Println(x)
			}
		}
	}

	calc(10, 2)
	calc(10, 0)

	if err != nil {
		log.Printf("error: %v\n", err)
	}
}
