package main

import "fmt"

func readLog(lines *[]string) {
	*lines = append(*lines, "log message")
}

func main() {
	var slices []int

	arr := [3]int{42, 56, 78}

	slices = arr[:2]
	slice2 := slices[1:]

	slices[0] *= 2

	fmt.Println(slices)
	fmt.Println(arr)
	fmt.Println(slice2)
	fmt.Printf("cap: %d\n", cap(slice2))
	slice2 = slice2[:len(slice2)+1]
	fmt.Println(slice2)

	slice3 := make([]int, 3, 10)
	copy(slice3[1:], slice2)
	fmt.Println(slice3, cap(slice3))

	slice3 = append(slice3, 88, 99, 102, 129, 239, 2398, 23789, 209)
	fmt.Println(slice3, cap(slice3))

	msgs := []string{
		"meine erste log message",
	}
	fmt.Printf("cap msg: %d\n", cap(msgs))

	readLog(&msgs)
	fmt.Printf("%+q\n", msgs)

}
