package main

import "fmt"

func main() {

	//var m map[string]int
	//m := make(map[string]int)
	m := map[string]int{
		"Petra": 89,
		"Egon":  0,
	}

	m["klaus"] = 23

	fmt.Println(m, m["Petra"])

	delete(m, "Petra")
	fmt.Println(m, m["Petra"], m["Egon"])

	egon, ok1 := m["Egon"]
	petra, ok2 := m["Petra"]

	fmt.Println(egon, ok1, petra, ok2)
	if _, ok := m["Petra"]; ok {
		fmt.Println("Petra ist drin")
	} else {
		fmt.Println("Petra ist nicht drin")
	}

	for k, v := range m {
		fmt.Printf("%s: %d\n", k, v)
	}
}
