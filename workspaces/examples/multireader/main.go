package main

import (
	"fmt"
	"sync"
)

func worker(wg *sync.WaitGroup, numbers <-chan int, id int) {
	defer wg.Done()

	for n := range numbers {
		fmt.Println(id, n)
	}
}

func main() {

	numbers := make(chan int)

	var wg sync.WaitGroup

	for i := 0; i < 5; i++ {
		wg.Add(1)
		go worker(&wg, numbers, i)
	}

	for i := 0; i < 100; i++ {
		numbers <- i
	}
	close(numbers)

	wg.Wait()
}
