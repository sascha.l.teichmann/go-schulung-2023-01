package main

import "fmt"

func greet() {
	fmt.Println("greet")
}

type greeter int

func (g greeter) hello() {
	fmt.Println("hello from greeter")
}

func callGreeting(greet func()) {
	greet()
}

func main() {
	callGreeting(greet)
	var g greeter
	g.hello()
	callGreeting(g.hello) // method value

	callGreeting(func() { g.hello() })

}
