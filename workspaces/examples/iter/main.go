package main

import "fmt"

func calculate(x int) func() (int, bool) {
	z := 67
	return func() (int, bool) {
		fmt.Println("z", z)
		z *= 3
		v := x
		x++
		return v, v < 10
	}
}

func main() {

	f := calculate(3)
	for x, ok := f(); ok; x, ok = f() {
		fmt.Println(x)
	}
}
