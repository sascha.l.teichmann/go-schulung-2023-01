package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type calculator float64

func (c *calculator) add(x float64) {
	*c += calculator(x)
}

// func sub(c *calculator, x float64) {
func (c *calculator) sub(x float64) {
	*c -= calculator(x)
}

func (c *calculator) mul(x float64) {
	*c *= calculator(x)
}

var operators = map[string]func(*calculator, float64){
	"+": (*calculator).add, // Method expression
	"-": (*calculator).sub,
	"*": (*calculator).mul,
}

func main() {

	var c calculator

	//  Method values
	/*
	 */

	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		opS, operantS, ok := strings.Cut(line, " ")
		if !ok {
			continue
		}
		v, err := strconv.ParseFloat(operantS, 64)
		if err != nil {
			log.Printf("invalid operant: %q\n", operantS)
			continue
		}
		if op := operators[opS]; op != nil {
			op(&c, v)
		}
		fmt.Println(c)
	}
	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
