package main

import "fmt"

func modify(arr *[2]rune) {
	arr[0] = 'A'
	arr[1] = 'B'
}

func badInC() *int {
	var foo int
	return &foo
}

func main() {
	var x int

	var p *int

	p = &x

	*p = 42

	fmt.Println(x, p)

	var arr [2]rune
	modify(&arr)
	fmt.Printf("%q\n", &arr)

	y := badInC()
	*y = 78

	z := new(int)
	*z = 68
}
