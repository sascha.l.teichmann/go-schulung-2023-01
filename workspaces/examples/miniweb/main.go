package main

import (
	"fmt"
	"net/http"
)

type controller struct {
	books []string
}

func (c *controller) listBooks(
	rw http.ResponseWriter,
	req *http.Request,
) {
	for i, b := range c.books {
		fmt.Fprintf(rw, "%d: %s\n", i+1, b)
	}
}

func main() {

	ctrl := controller{
		books: []string{"Bibel", "Koran"},
	}

	mux := http.NewServeMux()

	mux.HandleFunc("/books", ctrl.listBooks)

	mux.HandleFunc("/greet",
		func(rw http.ResponseWriter, req *http.Request) {
			fmt.Fprintln(rw, "Hello")
		})

	http.ListenAndServe(
		"localhost:8080",
		mux)
}
