package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
)

type myStruct struct {
	Num    int     `json:"number" xml:"Nummy"`
	Factor float64 `json:"factor"`
	Secret bool
}

func process(st myStruct) {
	fmt.Println(st)
}

func main() {

	var st = myStruct{
		Factor: 2,
		Num:    42,
	}

	st.Factor = 3.1415

	process(st)

	x, _ := json.Marshal(&st)
	fmt.Println(string(x))
	y, _ := xml.Marshal(&st)
	fmt.Println(string(y))
}
