package main

import "fmt"

type Auto interface {
	Fahren()
	Break
}

type Break interface {
	Bremsen()
}

func fahrenUndBremsen(a Auto) {
	a.Fahren()
	a.Bremsen()
}

type Pickup int

func (pu Pickup) Fahren() {
	fmt.Println("Vrooom!")
}

/*
func (pu Pickup) Bremsen() {
	fmt.Println("Quietsch")
}
*/

type Bremse float32

func (b Bremse) Bremsen() {
	fmt.Println("Quietsch (Bremse)")
}

func (pu Pickup) String() string {
	return fmt.Sprintf("motorleistung: %d", pu)
}

func main() {

	//var auto Auto
	pu := Pickup(10)
	b := Bremse(1.23)

	fahrenUndBremsen(struct {
		Pickup
		Break
	}{
		pu,
		b,
	})

	fmt.Println(pu)
}
