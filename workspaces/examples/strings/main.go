package main

import "fmt"

const text = `\n
Her
  is
text
` // here-documents

func main() {
	fmt.Println("💩")

	var s string
	s = "💩"
	fmt.Println(len(s))

	// intern: immutable byte slice

	// s[0] = 'x'

	bytes := []byte{'A', 'B', 'c'}

	s = string(bytes)

	fmt.Println(len(s))

	s = "AB💩CD"

	for i, c := range s {
		fmt.Printf("%d: %c\n", i, c)
	}

	for i := 0; i < len(s); i++ {
		fmt.Printf("index %d: %c\n", i, s[i])
	}

	runes := []rune(s)

	for i, c := range runes {
		fmt.Printf("rune %d: %c\n", i, c)
	}
}
