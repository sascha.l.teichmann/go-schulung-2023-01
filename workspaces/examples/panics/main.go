package main

import (
	"errors"
	"fmt"
	"log"
	"regexp"
)

var reg = regexp.MustCompile(`^[a-z]+$`)

func level1() {
	level2()
}

func level2() {
	level3()
}

func level3() {
	// Fehler!
	panic("Syntax fehler")
}

func Parse(src string) (result bool, err error) {

	defer func() {
		fmt.Println("Hello from defer")
		if x := recover(); x != nil {
			fmt.Println("Da ist was faul")
			err = errors.New("Bad!")
		} else {
			fmt.Println("alles in butter")
		}
	}()

	level1()

	return true, nil
}

func main() {
	//x := []int{}
	//fmt.Println(x[0])

	s := "ab1s"

	fmt.Println(reg.MatchString(s))

	if _, err := Parse("complex syntax"); err != nil {
		log.Printf("error: %v\n", err)
	}
}
