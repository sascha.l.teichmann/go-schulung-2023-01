package main

import "fmt"

var bigLookUpTable = [...]int{
	12981,
	1238913,
	13897,
}

func secret(a ...int) {
}

func printInts(prefix string, a ...int) {
	for _, x := range a {
		fmt.Println(prefix, x)
	}
	secret(a...)
}

func main() {
	slice := []int{1, 2, 4}
	printInts("hallo", 1, 2)
	printInts("hallo", slice...)

	nslice := append(slice, slice...)
	fmt.Println(nslice)
}
