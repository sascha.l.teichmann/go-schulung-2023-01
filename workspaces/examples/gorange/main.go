package main

import "fmt"

func main() {

	done := make(chan struct{})
	numbers := make(chan int)

	go func() {
		defer close(done)
		/*
			for i, v := range []int{42, 43} {
				fmt.Printf("%d: %d\n", i, v)
			}
		*/
		for n := range numbers {
			fmt.Println(n)
		}
	}()

	numbers <- 0
	numbers <- 1
	for i := 2; i < 10; i++ {
		numbers <- i
	}
	close(numbers)

	<-done
}
