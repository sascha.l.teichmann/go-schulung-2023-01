package main

import "fmt"

const (
	a = 1 << iota
	b
	c
	d
	e
)

const debug = false

func main() {
	const (
		one         = 1 // untypisierte Konstante
		two float32 = 2 // typisierte Konstante

		three = 1e10 * 2.0
	)

	var x int = one
	var k int
	var y float32 = one

	y = two
	// x = two

	y = three
	x = three

	_, _ = x, y

	// t := x; x = k; k = t
	x, k = k, x

	//var z int = 12

	z := float32(12.3) // float32

	u := 12.0

	u = 12.3

	m := 5

	const gruss = "hurra"

	o := false
	o = 1 < 2

	if m <= 5 {
		fmt.Println("else")

	} else if m > 5 {
		fmt.Println("else")
	} else {
		fmt.Println("not reached")
	}

	if debug {
		fmt.Println(a, b, c, d, e, z, u, m, o)
	}
}
