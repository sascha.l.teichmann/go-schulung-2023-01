package main

import "fmt"

func main() {

	ch1 := make(chan int)
	ch2 := make(chan int)

	go func() {
		defer close(ch1)
		for i := 0; i < 10; i++ {
			ch1 <- i
		}
	}()

	go func() {
		defer close(ch2)
		for i := 10; i < 30; i++ {
			ch2 <- i
		}
	}()

	var remain chan int
	for remain == nil {
		select {
		case x, ok := <-ch1:
			if ok {
				fmt.Println("ch1", x)
			} else {
				remain = ch2
			}
		case x, ok := <-ch2:
			if ok {
				fmt.Println("ch2", x)
			} else {
				remain = ch1
			}
		}
	}
	for x := range remain {
		fmt.Println("remain", x)
	}

}
