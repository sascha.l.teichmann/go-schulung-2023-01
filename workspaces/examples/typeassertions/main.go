package main

import "fmt"

func printme(x any) {

	// y := x.(int)
	//fmt.Printf("%T\n", y)

	//y, ok := x.(int)
	//fmt.Printf("%T %t\n", y, ok)

	switch y := x.(type) {
	case int:
		fmt.Printf("integer %T\n", y)
	case float64:
		fmt.Println("float64", y)
	default:
		fmt.Printf("%T\n", y)
	}
}

func main() {
	printme([]int(nil))
	printme(nil)
	printme(1)
	printme("hallo")
	printme(3.1415)
}
