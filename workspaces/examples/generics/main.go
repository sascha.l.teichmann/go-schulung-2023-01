package main

import (
	"cmp"
	"fmt"
)

func min[T cmp.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

type BinaryTree[T cmp.Ordered] struct {
	left  *BinaryTree[T]
	right *BinaryTree[T]
	value T
}

func (bt *BinaryTree[T]) inorder(visit func(t T)) {
	if bt == nil {
		return
	}
	bt.left.inorder(visit)
	visit(bt.value)
	bt.right.inorder(visit)
}

type myType int

func main() {
	fmt.Println(min(myType(10), myType(20)))
	fmt.Println(min("a", "b"))
	fmt.Println(min(19, 29))
	fmt.Println(min(19, 29.3))

	node := BinaryTree[int]{value: 10}

	fmt.Println(node)

	node.inorder(func(x int) {
		fmt.Printf("inorder callback: %d\n", x)
	})
}
