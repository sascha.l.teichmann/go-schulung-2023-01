package main

import "fmt"

func print(
	prefix string,
	array [20]rune,
) {
	for i := 0; i < len(array); i++ {
		fmt.Printf("%s %d: %c\n", prefix, i, array[i])
	}
	array[1] = 'B'
	fmt.Println(">>", array[1])

	for _, c := range array {
		fmt.Printf("%s %c\n", prefix, c)
	}

}

func main() {

	array := [20]rune{
		10: 'A', 12: 'B', 13: 'C', 15: 'D',
	}

	array[0] = 'A'

	print("bla", array)
	fmt.Println(">", array[1])

}
