# Agenda

## Go - Allgemein

- Was ist Go? Oder die Kunst des Weglassens.
    - Statisch und streng typisiert
    - kompiliert
    - Garbage Collected
    - "einfach", ["bekannt"](./images/roots.png) und "langweilig"

## I - Basics (Teil 1)

- Pakete / Module / Workspaces, Programmstruktur, Kommentare
- Funktionen und Aufrufe: `func`, Call by value
    - init-Funktionen
- Kontrollstrukturen und Syntax:
    - "Oberon mit C-Syntax" [Evolution of Go (Robert Griesemer)](https://talks.golang.org/2015/gophercon-goevolution.slide)
    - `if`, `for`
- Konstanten `const`: typsiert, untypisiert
    - [Bsp. time.go in package time](https://golang.org/pkg/time/#Duration)
    - [Iota](https://golang.org/ref/spec#Iota)
- Datenstrukturen:
    - Variablen `var`:
      - einfache: `int`, `float32`, `rune`, ...
          - [Numeric types](https://golang.org/ref/spec#Numeric_types)
      - zusammengesetzte:
          - Arrays
      - Kontrollstrukturen II: `range`
      - Pointer
- Funktionen II / Closures
- Slices
    - [Variadische Funktionen](https://de.wikipedia.org/wiki/Variadische_Funktion)
        - Bsp. für variadische Funktion [func Printf aus dem fmt Package](https://golang.org/pkg/fmt/#Printf)
    - Builtin-Funktionen: `append`, `copy`, `len`, `cap`, ...
    - [SliceTricks](https://github.com/golang/go/wiki/SliceTricks)
- Structs
    - struct-Tags
- Maps
    - [Presentation: Inside the Map Implementation (Keith Randall)](https://docs.google.com/presentation/d/1CxamWsvHReswNZc7N2HMV7WPFqS8pvlPVZcDegdC_T4/edit#slide=id.p)
    - [Video: Inside the Map Implementation (Keith Randall)](https://www.youtube.com/watch?v=Tl7mi9QmLns)
- `defer`, `panic`, `recover`
    - [Beispielaufgabe](https://gitlab.com/sascha.l.teichmann/go-examples/-/blob/master/methodexp/main.go)
- `return`: Tuple und Fehlerbehandlung
    - [Errors are values (Rob Pike)](https://blog.golang.org/errors-are-values)
- Operatoren

### Überhang
- Kontrollstrukturen: `switch`, `goto`

## II - Typsystem

- Typen
- Methoden, Method-Sets
- Reciever, Pointer-Reciever
- Interfaces, implizites Erfüllen
    - [implizites Erfüllen am Beispiel type Stringer im package fmt](https://golang.org/pkg/fmt/#Stringer)
- Embedding (Exkurs Objektorientierung mit anderen Mitteln)
    - Komposit-Gedanke
    - Vererbung mittels Embedding
- Kontrollstrukturen III:
    - Typ-Zusicherungen mittels Type-Assertions
    - type switch
      - [Beispiel aus dem Package fmt](https://golang.org/src/fmt/print.go#L623)
- Generics
    - Type Parameters für Funktionen und Typen
    - Type Sets durch Interfaces
    - Type Interference

# Überhang
- Brücke von Methoden zu Funktionen:
    - Method-Values
    - Method-Expressions
        - [Bsp. go-examples/methodexp/main.go](https://gitlab.com/sascha.l.teichmann/go-examples/-/blob/master/methodexp/main.go)
- `string`
    - Hinweise auf UTF8-Encoding
    - siehe auch [package unicode/utf8](https://golang.org/pkg/unicode/utf8/)

## III - Concurrency (Nebenläufigkeit)

- Einleitung
    - [Communicating Sequential Processes (CSP) von Tony Hoare](https://de.wikipedia.org/wiki/Communicating_Sequential_Processes)
    - [Concurrency is not parallelism (Rob Pike)](https://blog.golang.org/concurrency-is-not-parallelism)
    - [C10k problem](https://en.wikipedia.org/wiki/C10k_problem)
- Go-Routines `go f()`
- Channels `chan`
    - [Effective Go / channels](https://golang.org/doc/effective_go.html#channels)
- Kontrollstrukturen IV:
    - `range`, `<-`
    - `select`
- Orchestrierend vs. serialisierend (`sync`-Paket)
    - Orchestrierend
        - Channels `chan`
    - serialisierend
        - waitGroup
        - Mutex
        - atomic

## IV - Standard-Bibliothek (optional)

- `io`, `fmt`, `os`, ...

## TODO

- Reflection: Types, Values (Hinweis: Konzept der Pointer in Go muss verstanden sein!!!)


## VI: Meta-Programming (optional)

- Generierung von Code: `go generate`
- interne Data-Repräsentierung
- `unsafe`
